//
//  main.cpp
//  SimpleMemoryCardEmulation
//
//  Created by Sara Mahmoud on 1/29/20.
//  Copyright © 2020 Sara Mahmoud. All rights reserved.
//

#include <iostream>
#include "SimpleMemoryCardEmulation.cpp"
#include "QueueOperations.cpp"
#include "CommandManager.cpp"
#include "googleTest.hpp"

int main(int argc, const char * argv[]) {
    std::cout << "Memory Card Emulation started!\n";
    testing::InitGoogleTest();
    return RUN_ALL_TESTS();
}
