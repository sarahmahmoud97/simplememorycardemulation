//
//  CommandManagerFactory.cpp
//  MemoryCardEmulation
//
//  Created by Sara Mahmoud on 2/9/20.
//  Copyright © 2020 Sara Mahmoud. All rights reserved.
//

#include <stdio.h>

#include "CommandManager.hpp"
#include "CommandManagerException.hpp"

// Factory method to create objects of different types.
CommandManager::CommandManagerFactory* CommandManager::CommandManagerFactory::Create(Operation type) {
    if (type == READ)
        return new ReadOperation();
    else if (type == WRITE)
        return new WriteOperation();
    else return NULL;
}

Response* CommandManager::ReadOperation::executeCommand(uint32_t logicalAddress, uint32_t transferLength, Command command,
                                                   MemoryOperations *memoryOperationsInstance, uint8_t *tempBuffer) {
    
    logicalAddress = logicalAddress * LPA_SIZE;//start reading from logicalAddress
    transferLength = transferLength * LPA_SIZE;//number of bytes to read
    uint32_t buffer_size = command.buffer_size;
    ResponseStatus status = SUCCESS;
    Response *response = new Response;
    
    if (transferLength > (buffer_size*BUFFER_BLOCK_SIZE)) {
        throw MemoryCardEmulationException(EXCEEDS_BUFFER_SIZE);
    }
    
    if (validLogicalAddress(logicalAddress)) {
        int counter = 0;
        while (counter < transferLength) {
            tempBuffer[counter] = memoryOperationsInstance->memory[logicalAddress+counter];
            counter++;
        }
        cout << "temporal buffer" << endl;
        for (int i=0; i<buffer_size*BUFFER_BLOCK_SIZE;i++ ) {
            printf("%x\n", tempBuffer[i]);
        }
        command.buffer = (uint32_t *)tempBuffer;
        
        cout << endl << "buffer" << endl;
        for (int i=0; i<buffer_size;i++ ) {
            printf("%x\n", command.buffer[i]);
        }
        
    } else {
        status = INVALID_ADDRESS;
    }
    
    response->id = command.id;
    response->buffer_size = buffer_size;
    response->buffer = command.buffer;
    response->operation_type = command.operation_type;
    response->status = status;
    
    return response;
}

Response* CommandManager::WriteOperation::executeCommand(uint32_t logicalAddress, uint32_t transferLength, Command command, MemoryOperations *memoryOperationsInstance, uint8_t *tempBuffer) {
    
    //write the data in buffer to address in lpa
    /*
     
    uint32_t* buffer = command.buffer;
    while (counter < buffer_size) {
        counter++;
        if (validLogicalAddress(logicalAddress)) {
            if (!this->memoryOperationsInstance.memory[counter + logicalAddress])
                this->memoryOperationsInstance.memory[counter + logicalAddress] = buffer[counter];
            else
                throw MemoryCardEmulationException();// not empty we couldn't write to //TODO: throw exception
        } else {
            status = INVALID_ADDRESS;
            break;
        }
    }
    buffer = nullptr;
     */
    return NULL;
}
