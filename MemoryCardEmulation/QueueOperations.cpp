//
//  QueueOperations.cpp
//  MemoryCardEmulation
//
//  Created by Sara Mahmoud on 2/8/20.
//  Copyright © 2020 Sara Mahmoud. All rights reserved.
//

#include "QueueOperations.hpp"
#include "CommandManagerException.hpp"

bool QueueOperations::addCommandToQueue(Command &command) {
    bool result{false};
    uint32_t queue_size = this->getQueueSize();
    if (queue_size <= 0) {
        throw MemoryCardEmulationException(EXCEEDS_QUEUE_SIZE);
    } else {
        if(this->queued_operations.find(command.id) != this->queued_operations.end()) {
            throw MemoryCardEmulationException(FOUND_COMMAND_ID);
        } else { // not found
            this->queued_operations[command.id] = command;
            result=true;
            this->setQueueSize(--queue_size);
        }
    }
    return result;
}

bool QueueOperations::removeCommandFromQueue(uint32_t commandId) {
    bool result{false};
    uint32_t queue_size = this->getQueueSize();
    if (queue_size >= QUEUE_SIZE) {
        throw MemoryCardEmulationException(EMPTY_QUEUE);
    } else {
        if (!this->queued_operations.empty()) { // extra check
            if(this->queued_operations.find(commandId) == this->queued_operations.end()) {
                throw MemoryCardEmulationException(NOT_FOUND_COMMAND_ID);
            }
            this->queued_operations.erase(commandId);
            result=true;
            this->setQueueSize(++queue_size);
        }
    }
    return result;
}

uint32_t QueueOperations::getQueueSize() {
    return this->queue_size;
}

void QueueOperations::setQueueSize(uint32_t size) {
    this->queue_size = size;
}

map<uint32_t, Command> QueueOperations::getQueuedOperations() {
    return this->queued_operations;
}

QueueOperations* QueueOperations::instance = nullptr;

QueueOperations* QueueOperations::getInstance() {
    if (!instance)
        instance = new QueueOperations;
    return instance;
}
