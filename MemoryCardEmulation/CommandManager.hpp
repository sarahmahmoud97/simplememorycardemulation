//
//  CommandManager.hpp
//  MemoryCardEmulation
//
//  Created by Sara Mahmoud on 2/8/20.
//  Copyright © 2020 Sara Mahmoud. All rights reserved.
//

#ifndef CommandManager_hpp
#define CommandManager_hpp
#include "SimpleMemoryCardEmulation.hpp"
#include "QueueOperations.hpp"

#define BUFFER_BLOCK_SIZE 4

//This class represents the Ordered execution of the commands in the Queue by writing to or reading from the memory
class CommandManager {
private:
    static CommandManager *instance;
    QueueOperations* queueInstance;
    Response* respons;
    uint8_t* temporalBuffer;
    class MemoryOperations {
        public:
        uint8_t memory[MAX_MEMORY_SIZE]{1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8};// 1d array of 1M (1024) which represents the memory
        MemoryOperations() {
            for(int it=26;it< MAX_MEMORY_SIZE; it++){
                memory[it] =5;
            }
        }
    };
    
    class CommandManagerFactory {
    public:
        virtual Response* executeCommand(uint32_t logicalAddress, uint32_t transferLength, Command command, MemoryOperations *memoryOperationsInstance, uint8_t *tempBuffer) = 0;
        static CommandManagerFactory* Create(Operation type);
    };

    class ReadOperation : public CommandManagerFactory {
    public:
        Response* executeCommand(uint32_t logicalAddress, uint32_t transferLength, Command command, MemoryOperations *memoryOperationsInstance, uint8_t *tempBuffer);
    };

    class WriteOperation : public CommandManagerFactory {
    public:
        Response* executeCommand(uint32_t logicalAddress, uint32_t transferLength, Command command, MemoryOperations *memoryOperationsInstance, uint8_t *tempBuffer);
    };
    
    CommandManager() {
        queueInstance = queueInstance->getInstance();
    }
    
    CommandManager::MemoryOperations memoryOperationsInstance;
    
public:
    static CommandManager *getInstance();
    static bool validLogicalAddress(uint32_t logicalAddress);
    void executeCommandsInQueue(uint8_t* tempBuffer);
    Response* getResponse();
    void setResponse(Response* response);
    std::function<Response()> bindCallBackFunction();
    Response callBackFunction();
    uint8_t* getTemporalBuffer();
    QueueOperations* getQueueInstance();
    ~CommandManager() {
        respons = nullptr;
        delete queueInstance;
    }
};
#endif /* CommandManager_hpp */
