//
//  SimpleMemoryCardEmulation.hpp
//  SimpleMemoryCardEmulation
//
//  Created by Sara Mahmoud on 1/29/20.
//  Copyright © 2020 Sara Mahmoud. All rights reserved.
//

#ifndef SimpleMemoryCardEmulation_hpp
#define SimpleMemoryCardEmulation_hpp

#include <stdio.h>

#include<iostream>
using namespace std;

#include <exception>
#include <string>
#include <queue>
#include <gtest/gtest.h>
#include <list>

#define MAX_MEMORY_SIZE 1024
#define QUEUE_SIZE 10
#define LPA_SIZE 2

enum OperationType {
    COMMAND=1,
    RESPONSE=2
};

enum Operation {
    READ=0x20,
    WRITE=0x30
};

enum ResponseStatus {
    INVALID_OPCODE=0x01,
    INVALID_TR=0x02,
    INVALID_ADDRESS=0x03,
    SUCCESS=0x00
};

//Command Packet, order is very omportant
struct Command {
    OperationType operation_type; // 1 byte
    uint16_t id;// unsigned , normal counter 2 bytes
    Operation op_code;// op-code >> hex value 1 byte
    uint32_t transferLength; //actual bytes will be filled in the buffer, check the buffer size
    uint32_t lba;// LBA (Logical block addressing) unsigned 32 int
    uint32_t buffer_size; //anything that not allowed to be negative should be unsigned
    uint32_t *buffer;//buffer array of bytes hex , unsigned
    
    ~Command() {
        buffer = nullptr;
    }
};

//Response packet
struct Response {
    OperationType operation_type; // 1 byte
    uint16_t id; // 2 bytes
    ResponseStatus status; // it should be hex not string 1 byte
    uint32_t buffer_size;
    uint32_t *buffer;
    
    ~Response() {
        buffer = nullptr;
    }
};

#endif /* SimpleMemoryCardEmulation_hpp */
