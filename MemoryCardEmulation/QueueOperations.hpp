//
//  QueueOperations.hpp
//  MemoryCardEmulation
//
//  Created by Sara Mahmoud on 2/8/20.
//  Copyright © 2020 Sara Mahmoud. All rights reserved.
//

#ifndef QueueOperations_hpp
#define QueueOperations_hpp
#include "SimpleMemoryCardEmulation.hpp"

class QueueOperations {
private:
    map<uint32_t, Command> queued_operations;
    uint32_t queue_size;
    static QueueOperations *instance;
    
    QueueOperations() {
        queue_size = QUEUE_SIZE;
    }
    
public:
    static QueueOperations *getInstance();
    bool addCommandToQueue(Command &command);
    bool removeCommandFromQueue(uint32_t commandId);
    uint32_t getQueueSize();
    void setQueueSize(uint32_t size);
    map<uint32_t, Command> getQueuedOperations();
    ~QueueOperations() {
        
    }
};
#endif /* QueueOperations_hpp */
