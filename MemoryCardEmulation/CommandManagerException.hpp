//
//  CommandManagerException.hpp
//  MemoryCardEmulation
//
//  Created by Sara Mahmoud on 2/10/20.
//  Copyright © 2020 Sara Mahmoud. All rights reserved.
//

#ifndef CommandManagerException_h
#define CommandManagerException_h

#include <stdio.h>

#include<iostream>
using namespace std;

#include <exception>
#include <string>

enum ExceptionType{
    EXCEEDS_QUEUE_SIZE,
    EXCEEDS_BUFFER_SIZE,
    EMPTY_QUEUE,
    FOUND_COMMAND_ID,
    NOT_FOUND_COMMAND_ID
};


class MemoryCardEmulationException : public exception {
public:
    ExceptionType exception_type;
    map<ExceptionType, const char*> exceptionTypesMap;
    
    MemoryCardEmulationException(ExceptionType exception_type) {
        this->exception_type = exception_type;
        exceptionTypesMap[EXCEEDS_QUEUE_SIZE] = "The queue sotrage can only handle 10 commands, you exceed it!";
        exceptionTypesMap[EXCEEDS_BUFFER_SIZE] = "You exceed the buffer size!";
        exceptionTypesMap[EMPTY_QUEUE] = "Empty Queue you cannot remove!";
        exceptionTypesMap[FOUND_COMMAND_ID] = "Command id is already exist, it should be unique";
        exceptionTypesMap[NOT_FOUND_COMMAND_ID] = "Command id is not exist, you can't remove";
    }
    
    const char * what () const throw () {
        return this->exceptionTypesMap.find(exception_type)->second;
    }
};

#endif /* CommandManagerException_h */
