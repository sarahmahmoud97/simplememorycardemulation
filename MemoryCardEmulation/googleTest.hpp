//
//  googleTest.hpp
//  MemoryCardEmulation
//
//  Created by Sara Mahmoud on 2/6/20.
//  Copyright © 2020 Sara Mahmoud. All rights reserved.
//

#ifndef googleTest_hpp
#define googleTest_hpp

#include <exception>
#include <stdio.h>
#include "CommandManagerException.hpp"

class MemoryCardEmulationTest: public testing::Test {
protected:
    CommandManager* commandManagerInstance = commandManagerInstance->getInstance();
    QueueOperations* queueOperationsInstance = commandManagerInstance->getQueueInstance();
    MemoryCardEmulationTest() {}
    ~MemoryCardEmulationTest() {}
    virtual void setup() {};
    virtual void teardown() {};
    
};


TEST_F(MemoryCardEmulationTest, removeFromEmptyQueue) {
    uint32_t command_id = 2;
    try {
        queueOperationsInstance->removeCommandFromQueue(command_id);
        FAIL() << "Expected Empty Queue you cannot remove!";
    }
    catch(MemoryCardEmulationException& err) {
        EXPECT_EQ(err.what(),"Empty Queue you cannot remove!");
    }
    catch(...) {
        FAIL() << "Expected Empty Queue you cannot remove!";
    }
    //EXPECT_FALSE(queueOperationsInstance->removeCommandFromQueue(command_id));
}

TEST_F(MemoryCardEmulationTest, removeFromNonEmptyQueue) {
    Command command;
    command.operation_type = COMMAND;
    command.transferLength=8; //8 logical blocks
    command.op_code = READ;
    command.buffer_size = 4;
    command.lba = 2;
    command.id = 1;
    EXPECT_TRUE(queueOperationsInstance->addCommandToQueue(command));
    
    EXPECT_TRUE(queueOperationsInstance->removeCommandFromQueue(command.id));
}

//tested with both LPA_SIZE =1, =2
TEST_F(MemoryCardEmulationTest, addCommandToQueueAndExecute) {
    //two commands with valid addresses
    Command command;
    command.operation_type = COMMAND;
    command.transferLength=8; //8 logical blocks
    command.op_code = READ;
    command.buffer_size = 4;
    command.lba = 2;
    command.id = 1;
    
    Command secondCommand;
    secondCommand.operation_type = COMMAND;
    secondCommand.transferLength=8;
    secondCommand.op_code = READ;
    secondCommand.buffer_size = 4;
    secondCommand.lba = 5;
    secondCommand.id = 2;
    
    EXPECT_TRUE(queueOperationsInstance->addCommandToQueue(command));
    EXPECT_TRUE(queueOperationsInstance->addCommandToQueue(secondCommand));
    
    std::function<Response()> f_get_responses = commandManagerInstance->bindCallBackFunction();
    
    uint8_t* tempBuffer = new uint8_t[(secondCommand.buffer_size)*BUFFER_BLOCK_SIZE];
    
    commandManagerInstance->executeCommandsInQueue(tempBuffer);
    //Response response = f_get_responses();
    Response expectedResponse;
    expectedResponse.operation_type = RESPONSE;
    expectedResponse.buffer_size = 4;
    expectedResponse.id = 1;
    expectedResponse.status = SUCCESS;
    uint32_t bufferArray[] = {0x8070605,0x3020109,0x7060504,0x2010908};
    expectedResponse.buffer=bufferArray;
    
    Response expectedResponse2;
    expectedResponse2.operation_type = RESPONSE;
    expectedResponse2.buffer_size = 4;
    expectedResponse2.id = 2;
    expectedResponse2.status = SUCCESS;
    uint32_t bufferArray2[] = {0x5040302,0x9080706,0x4030201,0x8070605};
    expectedResponse2.buffer=bufferArray2;

    
    /*
     list<Response> expectedResponses;
     expectedResponses.push_back(expectedResponse);
     expectedResponses.push_back(expectedResponse2);
     list<Response>::iterator it;
     list<Response>::iterator it2;
     
    for (it = responses.begin(), it2= expectedResponses.begin(); it != responses.end() && it2 != expectedResponses.end(); ++it, ++it2) {
        //make sure that the command Id is same as the response id and success status
        EXPECT_EQ(it2->id, it->id);
        cout << it->id <<endl;
        cout << it->status <<endl;
        EXPECT_EQ(it->status, it2->status);
        
        // we need also to check the buffer
        for (int i=0; i<it->buffer_size;i++ ) {
            //printf("%x\n", it->buffer[i]);
            EXPECT_EQ(it->buffer[i], it2->buffer[i]);
        }
    }*/
    
}


TEST_F(MemoryCardEmulationTest, addMoreThanQueueSize) {
    Command firstCommand;
    firstCommand.operation_type = COMMAND;
    firstCommand.transferLength=8; //10 logical blocks
    firstCommand.op_code = READ;
    firstCommand.buffer_size = 4;
    firstCommand.lba = 2;
    firstCommand.id = 1;
    
    Command command2; command2.id = 2;
    Command command3; command3.id = 3;
    Command command4; command4.id = 4;
    Command command5; command5.id = 5;
    Command command6; command6.id = 6;
    Command command7; command7.id = 7;
    Command command8; command8.id = 8;
    Command command9; command9.id = 9;
    Command command10; command10.id = 10;
    
    EXPECT_TRUE(queueOperationsInstance->addCommandToQueue(firstCommand));
    EXPECT_TRUE(queueOperationsInstance->addCommandToQueue(command2));
    EXPECT_TRUE(queueOperationsInstance->addCommandToQueue(command3));
    EXPECT_TRUE(queueOperationsInstance->addCommandToQueue(command4));
    EXPECT_TRUE(queueOperationsInstance->addCommandToQueue(command5));
    EXPECT_TRUE(queueOperationsInstance->addCommandToQueue(command6));
    EXPECT_TRUE(queueOperationsInstance->addCommandToQueue(command7));
    EXPECT_TRUE(queueOperationsInstance->addCommandToQueue(command8));
    EXPECT_TRUE(queueOperationsInstance->addCommandToQueue(command9));
    EXPECT_TRUE(queueOperationsInstance->addCommandToQueue(command10));
    
    try {
        queueOperationsInstance->addCommandToQueue(firstCommand);
        FAIL() << "ExceedSizeOfQueue";
    }
    catch(MemoryCardEmulationException& err) {
        EXPECT_EQ(err.what(),"The queue sotrage can only handle 10 commands, you exceed it!");
    }
    catch(...) {
        FAIL() << "Expected : The queue sotrage can only handle 10 commands, you exceed it!";
    }
    //EXPECT_FALSE(queueOperationsInstance->addCommandToQueue(firstCommand)); //should throw an exception
}


TEST_F(MemoryCardEmulationTest, passInvalidLBA) {
    Command command;
    command.operation_type = COMMAND;
    command.transferLength=8;
    command.op_code = READ;
    command.buffer_size = 4;
    command.lba = 600;
    command.id = 2;
    
    EXPECT_TRUE(queueOperationsInstance->removeCommandFromQueue(1));
    EXPECT_TRUE(queueOperationsInstance->removeCommandFromQueue(2));
    EXPECT_TRUE(queueOperationsInstance->addCommandToQueue(command));
    
    std::function<Response()> f_get_responses = commandManagerInstance->bindCallBackFunction();
    
    uint8_t* tempBuffer = new uint8_t[(command.buffer_size)*BUFFER_BLOCK_SIZE];
    commandManagerInstance->executeCommandsInQueue(tempBuffer);
    Response response = f_get_responses();
    
    Response expectedResponse;
    expectedResponse.operation_type = RESPONSE;
    expectedResponse.buffer_size = 4;
    expectedResponse.id = 2;
    expectedResponse.status = INVALID_ADDRESS;
    
    
    /*
    for (it = responses.begin(), it2= expectedResponses.begin(); it != responses.end() && it2 != expectedResponses.end(); ++it, ++it2) {
        EXPECT_EQ(it2->id, it->id);
        EXPECT_EQ(it->status, it2->status);
    }
     */
}

#endif /* googleTest_hpp */
