//
//  CommandManager.cpp
//  MemoryCardEmulation
//
//  Created by Sara Mahmoud on 2/8/20.
//  Copyright © 2020 Sara Mahmoud. All rights reserved.
//

#include "CommandManager.hpp"

bool CommandManager::validLogicalAddress(uint32_t logicalAddress) {
    bool isValid = false;
    //we have a memory addresses within the range 0 -> 1023 when LPA_SIZE = 1
    uint validAddresses = (MAX_MEMORY_SIZE/LPA_SIZE);
    if (logicalAddress > 0 && logicalAddress < validAddresses)
        isValid = true;
    return isValid;
}

//call back function will return the response to the host
std::function<Response()> CommandManager::bindCallBackFunction() {
     std::function<Response()> f_get_response = std::bind(&CommandManager::callBackFunction, this); // register callBack fn
    return f_get_response;
}

Response CommandManager::callBackFunction() {
    return *(this->getResponse());
}

CommandManager* CommandManager::instance = nullptr;

CommandManager* CommandManager::getInstance() {
   if (!instance)
       instance = new CommandManager;
   return instance;
}

void CommandManager::executeCommandsInQueue(uint8_t* tempBuffer) {
    QueueOperations* queueInstance = this->queueInstance;
    map<uint32_t, Command>::iterator it;
    
    map<uint32_t, Command> queuedOperations = queueInstance->getQueuedOperations();
    list<ResponseStatus> statuses;
    Response *response = nullptr;
    ResponseStatus status = SUCCESS;
    uint32_t logicalAddress;
    uint32_t transferLength;
    
    for ( it = queuedOperations.begin(); it != queuedOperations.end(); it++ ) {
        Command command = it->second;
        logicalAddress = command.lba;
        transferLength = command.transferLength;
        
        if (command.operation_type == COMMAND) {
            
            CommandManager::CommandManagerFactory* commandManagerFactoryInstance = CommandManagerFactory::Create(command.op_code);
            
            if (commandManagerFactoryInstance == NULL) {
                status = INVALID_OPCODE;
            } else {
                response = commandManagerFactoryInstance->executeCommand(logicalAddress, transferLength, command, &(this->memoryOperationsInstance), tempBuffer);
            }
        } else {
            status = INVALID_TR;
        }
        
        if (status == INVALID_TR) {
            response->status = INVALID_TR;
        }
        
        //remove the command from the queue after executing it
        queueInstance->removeCommandFromQueue(command.id);
        
        this->setResponse(response);
        this->callBackFunction();
    }
    
    queueInstance = nullptr;
}

uint8_t* CommandManager::getTemporalBuffer() { return this->temporalBuffer; }
Response* CommandManager::getResponse() { return this->respons;}
void CommandManager::setResponse(Response* response) { this->respons = response; }
QueueOperations* CommandManager::getQueueInstance() { return this->queueInstance; }
